{-
Hi, here is all you need to know on how to get started with Tidal.
Tutorial : https://tidalcycles.org/index.php/Tutorial
Syntax : https://tidalcycles.org/index.php/Sequence_parser_syntax

- Starting Tidal :
  Once Tidal is installed, start SuperCollider (add `SuperDirt.start` to your
  startup file).
  Start Atom with tidalcycles plugin, create a .tidal file, Boot tidalcycles
  (in Menu Packages/TidalCycles/Boot ...). You may need to manually set up GHCI
  path in package settings. (mine is ~/.ghcup/bin/ghci, to know this type
  `whereis ghci` or `which ghci` in a terminal.)

- Using Tidal :
  To evaluate a line of code, press Maj+Enter. You need to do this line by line.
  You can evaluate a multi-line code by selecting it and pressing Cmd+Enter.
-}
-- `hush` : Stop making sound :  (Or go to Packages/TidalCycles/Hush)
hush

-- `$` : Haskell Function. Pass a parameter to the left function
-- https://tidalcycles.org/index.php/Understanding_the_$

-- `sound`,
-- `s` : Make a note that repeats every cycle :
d1 $ sound "bd" -- d1 is 1 of 16 SuperDirt connections
d1 $ s "bd"

-- print the pattern  in the console
-- Useful for debugging
sound "bd bd" -- removed "d1 $", or other "once $"

-- For a list of SuperDirt samples (bd, sn, etc.) see SAMPLES.tidal

-- `silence` : Silence it :
d1 $ silence

-- `p` : Make a named pattern :
p "simple_pattern" $ sound "bd"
p "simple_pattern" $ silence

-- `:`,
-- `n`,
-- `samples` : Choose a specific sample :
d1 $ sound "bd:4" -- 0 to ???    it cycles if max is exceeded
d1 $ sound "bd" # n 4
d1 $ sound $ samples "sn sn sn sn" "0 1 2 3"

-- Play several sounds during cycle :
d1 $ sound "bd sn"
d1 $ sound "bd hh sn hh"

-- Play several patterns at the same time :
d1 $ sound "bd sn"
d2 $ sound "hh hh hh hh"

-- `solo` /
-- `unsolo` a track :
solo 1
unsolo 1

-- `!` : Replicate
d2 $ sound "hh!4" -- same as "hh hh hh hh"

-- `*` : Multiplication. ≠ `!`
d2 $ sound "hh*2 cp" -- same as "[hh hh] cp"

-- `/` : Division :
d1 $ sound "bd sn/2" -- Plays the snare one every 2nd cycle

-- `~` : Rest :
d2 $ sound "~ hh hh hh"

-- `_`,
-- `@n` : Elongate sample. ≠ `~`
d1 $ s "sd _ _ sd sd _" -- sicilian
d1 $ s "sd@3 sd sd@2"

-- `[]`,
-- `.` : Pattern within a pattern :
d3 $ sound "cp [cp cp cp]"
d3 $ sound "[bd bd] [bd [sd [sd sd] sd] sd]"
d3 $ sound "cp . cp cp cp"
d3 $ silence

-- `,` : Other way to layer several patterns.
d2 $ silence
d1 $ sound "[bd sn, hh*4]"

-- `<>` : Different steps each cycle :
d3 $ sound "<arpy:0 arpy:1 arpy:2 arpy:3>"

-- `{}`
-- `%` : Polymeters
d1 $ sound "{cp sn sn sn, arpy bass2 drum notes can}"
  -- above : 5 notes spread accross 4, it shifts every mesure.
  -- https://music.stackexchange.com/questions/10488/polymeter-vs-polyrhythm
d1 $ s "{arpy bass2 drum notes can}%4"

-- `(n, m)`,
-- `euclid n m` : Distribute n sounds accross m steps
d1 $ sound "arpy(5,8)"
  -- third parameter rotates the pattern :
  d1 $ sound "arpy(5,8,2)"
d3 $ euclid 5 8 $ sound "arpy"
  -- explanation ?
  -- I think it rounds n/m
  -- 5/8 = 1.6,       5 times :
  -- 0, 1.6, 3.2, 4.8, 6.4
  -- 0, 2, 3, 5, 6
  -- advanced :
  d1 $ euclid 3 8 $ sound "bd*2 [sn:3 cp]" -- repeats 2nd pattern over 1st.
  d1 $ sound "sn([5 3]/2,8)" -- 5,8 then 3,8

-- `..`,
-- 'run' : Range
d3 $ n "[0 .. 7] [3 .. 1]" # sound "supergong"
d3 $ n (run 7) # sound "supergong"
d3 $ silence

-- `~>`,
-- `<~` : Time shift (in cycles)
d1 $ (0.25 <~) $ sound "bd*2 cp*2 hh sn:1"
d1 $ every 4 (0.25 <~) $ sound "bd*2 cp*2 hh sn"
d1 $ "[0 0.25]/4" <~ (sound "bd*2 cp*2 hh sn")
d1 $ "0 0.02 0 0" <~ (s "cp cp cp cp")

-- Functions:
-- `rev` : Reverse a pattern
d1 $ rev (sound "[bd sn:3 bd cp, hh*7 ~]")

-- `slow` : slow down pattern
d1 $ slow 2 $ sound "[bd sn, hh*4]"

-- `fast`,
-- `density`
d1 $ fast 2 $ sound "[bd sn, hh*4]"

-- `every` : Every n cycles, do...
d1 $ every 4 (rev) (sound "[bd sn:3 bd cp, hh*7 ~]")
d1 $ every 4 (fast 2) (sound "[bd sn, hh*4]")
d1 $ sound (every 4 (fast 4) "bd*2 [bd [sn sn*2 sn] sn]")

-- `once` : Run something over 1 cycle only
once $ s "bd sd(3,8)"

-- `#` : Control Values
-- `crush` : bitcrushing
d1 $ sound "[bd sn, hh*4]" # crush 4

-- `speed` : Sample playback speed, increases/decreases pitch (≠ slow & fast)
d1 $ sound "[bd sn, hh*4]" # speed 2
d1 $ every 2 (# speed 2) $ sound "arpy*4" # speed 1

  -- combined :
  d1 $ sound "[bd sn, hh*4]" # crush 4 # speed 0.5
  -- pattern (needs to be quoted) :
  d1 $ sound "[bd*4]" # speed "1 2 3 4"
  d1 $ sound "arpy" # speed "[1, 1.5]"
  -- reverse sample :
  d1 $ speed "-1 -0.5 -2 -1.5" # sound "bd"

-- `gain`
d1 $ sound "bd*4" # gain "1 0.8 0.5 0.7"

-- Order : which comes first gives the pattern
d1 $ gain "1 0.8 0.5 0.7" # sound "bd"
  -- That means that the leftmost pattern might delete other patterns' steps
  -- I guess we can see `|>` as : left > right.

-- Control operators :
-- `|>`  =  `#` : changes the control value
d1 $ every 2 (|> speed 2) $ sound "arpy*4" |> speed 1

-- `|+` : Adds to the control value
d1 $ every 2 (|+ speed 1) $ sound "arpy*4" |> speed 1

-- `|*` : Multiply the CVal
d1 $ every 2 (|* speed 1.5) $ sound "arpy*4" |> speed 1

-- `|-` : Substract
-- `|/` : Divide, you get it

-- `>|` : reverse order in CV change
d1 $ sound "drum" >|  n "0 1*2 ~ 4"

-- `|>|` : Wuuuuut
d1 $ s "sn cp" |>|  n "[0 .. 2]"

-- `pan` :  Pan from right (0) to left (1)
d1 $ sound "sn*4" # pan "0 1"

-- `shape` : some amplifier... ???
d1 $ sound "[bd, sn*4]" # shape "0 0.25 0.5 0.75"

-- `vowel` : Formant filter, values a e i o u
d1 $ sound "sn*5" # vowel "a e i o u"

-- `off` : ??

-- `up` : Sample speed change based on 12-semi-tone scale.
d1 $ up "[0 .. 11]" # sound "arpy"

-- `setcps` : Cycles Per Second => BPM/60/4. Default is 120bpm = 0.5
setcps 0.45
setcps (140/60/4)

-- `cps` : same, as a control
d1 $ sound "cp(3,8)" # cps (slow 8 $ range 0.8 1.6 saw)

-- Oscillation Functions :
-- `sine`,
-- `saw`,
-- `tri`,
-- `square`
d1 $ sound "sn*16" # pan sine

-- Control CV with osc function and slow/fast
d1 $ sound "sn*16" # pan (slow 2 $ tri)
d1 $ sound "sn*16" # speed (fast 1.2 $ tri)

-- `range` : to scale oscillation functions
d1 $ sound "sn*16" # speed (range 1 5 $ tri)
  d1 $ sound "sn*16" # speed (slow 4 $ range 1 5 $ tri)
  d1 $ sound "sn*16" # speed (range (-2) 3 $ tri) -- negative values in ()

-- `cutoff` : lowpass filter
d1 $ sound "hh*16" # cutoff (range 300 1000 $ slow 2 $ sine) # resonance "0.7"

-- `rand` : Random
d1 $ sound "arpy*4" # speed (range 0.25 0.75 $ rand)

-- `irand` : Random integer
d1 $ s "arpy*8" # n (irand 30)
d1 $ sound "arpy*4" # up (irand 3)*4

-- `?`
-- `degrade` : 50/50% chance randomness
d1 $ sound "bd? sd? sd? sd?"
d1 $ s "hh*8?"
d1 $ sound "bd sn? cp hh?"
d1 $ sound "[bd sn cp hh]?"
d1 $ degrade $ sound "bd*16"

-- `cut` : stop sample overlap. (So much cleaner...)
d1 $ sound (samples "arpy*8" (run 8)) # speed "0.25" # cut "1"
  -- careful when using several SuperDirt connections (d2), use cut "2", "3"...
  -- same with stack
  d1 $ stack [s (samples "arpy*8" (run 8)) # speed "0.25" # cut "1", s (samples "bass2*6" (run 6)) # speed "0.5" # cut "2" ]

-- `whenmod` : apply a condition depending on loop number. Similar to `every`.
d1 $ whenmod 8 6 (rev) $ sound "bd*2 arpy*2 cp hh*4"
  -- takes n (loop number) / 8, and applies condition (rev) if n % 8 ≥ 6

-- `const` : completely replace a pattern
d1 $ const (sound "arpy*3") $ sound "bd sn cp hh"
d1 $ whenmod 8 6 (const $ sound "arpy(3,8) bd*4") $ sound "bd sn bass2 sn"
d1 $ every 12 (const $ sound "bd*4 sn*2") $ sound "bd sn bass2 sn"

-- `fastcat` : concatenate patterns into a cycle. (Serial)
d1 $ fastcat [s "bd sn:2" # vowel "[a o]/2", s "casio casio:1 casio*2"]

-- `cat`,
-- `slowcat` : concatenate patterns (each pattern = 1 cycle)
d1 $ cat [s "bd sn:2" # vowel "[a o]/2", s "casio casio:1 casio*2", s "drum drum:2 drum:3 drum:4*2"]

-- `randcat` : read a random pattern from list.

-- `stack` : concatenate patterns in parallel
d1 $ stack [s "bd bd*2", s "hh*2 [sn cp] cp future*4", s (samples "arpy*8" (run 16))]
  -- You can apply functions to the entire stack (every...)

-- Transitions :
-- `anticipate` : ????
d1 $ sound (samples "hh*8" (iter 4 $ run 4))
anticipate 1 $ sound (samples "bd(3,8)" (run 3))

-- `xfadeIn` : Cross-Fade over m cycles (here 16)
xfadeIn 1 16 $ sound "bd(5,8)"

-- other functions : xfade

-- Midi
-- SuperDirt comes with synths
-- For a list of Synths see MIDI_SYNTHS.tidal
-- `midinote` : you get it. use sound with synth name
d1 $ midinote "60 62*2" # s "supersaw"

-- `n` : with note names. Add s or f after note letter for semitones.
d1 $ n "c5 d5*2" # s "supersaw"
d1 $ n "<[a5,cs5,e5,g5]*3 [d5,fs5,g5,c5]>" # s "supersquare" # gain "0.7"
d2 $ every 4 (rev) $ n "<[g5 df5 e5 a5] [gf5 d5 c5 g5]*3>" # s "supersaw"
d2 $ silence
  -- wow this is getting interesting...
  -- n also works with numbers, 0 is c5 (midinote 60)

-- `sustain` : sustain...
d1 $ n "c5 d5*2" # s "supersaw" # sustain "0.4 0.2"

-- `resetCycles` : reset to cycle begin
resetCycles

-- `trigger` : reset this specific channel
d1 $ trigger 1 $ s "arpy*8" # n (irand 8)

-- `qtrigger` : quantized reset
d1 $ qtrigger 1 $ s "arpy*8" # n (irand 8) # speed envL

-- `superimpose` : new command stacks on top of what was previously there.
d1 $ superimpose -- blabla

-- `swing`,
-- `swingby` : see corresponding page. Delays every 2nd element by %

-- Questions :
-- How to sync to cycle begin ? qtrigger ?
-- Other transition functions
-- Custom functions -> that's Haskell stuff
-- Concatenative synth ? (granular) --> chop : https://tidalcycles.org/index.php/Manipulating_samples
-- import samples in SuperDirt wo copying folder

-- Thoughts
-- Link with MuBu concatenative synthesis would be cool

-- directly live code in SuperCollider ? here is a video :
-- https://www.youtube.com/watch?time_continue=205&v=xXNB1BbKY8A

-- Misc :
d1 $ silence
d1 $ s "arpy*4" # up "-3 5 3 5" # cps 1.2
d1 $ sound "hh:9(7,16,14)" -- Bossa rhythm
d1 $ n (off 0.125 (+12) $ off 0.25 (+7) $ slow 2 $ "0(3,8) [5 7]")
  # sound "supergong"
d1 $ sound "sn:2*16" # speed ((range 0.5 3 sine) * (slow 4 saw))

d1 $ sound "bd*2 [[~ lt] sn:3] lt:1 [ht mt*2]"
